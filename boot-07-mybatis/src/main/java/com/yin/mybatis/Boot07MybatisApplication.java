package com.yin.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot07MybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot07MybatisApplication.class, args);
    }

}
