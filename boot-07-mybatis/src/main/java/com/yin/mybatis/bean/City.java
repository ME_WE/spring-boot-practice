package com.yin.mybatis.bean;

import lombok.Data;

/**
 * City
 *
 * @author Yin Guiqing
 */
@Data
public class City {
    private Long id;
    private String name;
    private String state;
    private String country;
}
