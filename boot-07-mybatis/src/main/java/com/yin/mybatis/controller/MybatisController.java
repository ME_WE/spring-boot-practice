package com.yin.mybatis.controller;

import com.yin.mybatis.bean.City;
import com.yin.mybatis.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * MybatisController
 *
 * @author Yin Guiqing
 */
@Controller
public class MybatisController {
    @Autowired
    CityService cityService;

    @ResponseBody
    @GetMapping("/city")
    public City getCityById(@RequestParam("id") Long id) {
        return cityService.getCityById(id);
    }

    @ResponseBody
    @PostMapping("/city")
    public City insertCity(City city) {
        cityService.insert(city);
        return city;
    }
}
