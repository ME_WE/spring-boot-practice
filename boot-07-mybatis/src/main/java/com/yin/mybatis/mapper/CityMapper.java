package com.yin.mybatis.mapper;

import com.yin.mybatis.bean.City;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * CityMapper
 *
 * @author Yin Guiqing
 */
@Mapper
@Repository
public interface CityMapper {
    /*
      简单的SQL直接注解搞定，复杂的SQL在xml中编写
     */
    /**
     * 根据id查询city
     *
     * @param id id
     * @return 查询到的city
     */
    @Select("select id, name, state, country from city where id=#{id}")
    City getCityById(Long id);

    /**
     * 插入一条city记录
     *
     * @param city 要插入的city
     */
    @Insert("insert into city(name, state, country) values(#{name},#{name},#{name})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(City city);
}
