package com.yin.mybatis.service;

import com.yin.mybatis.bean.City;
import com.yin.mybatis.mapper.CityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * CityService
 *
 * @author Yin Guiqing
 */
@Service
public class CityService {
    @Autowired
    CityMapper cityMapper;

    public City getCityById(Long id) {
        return cityMapper.getCityById(id);
    }

    public void insert(City city) {
        cityMapper.insert(city);
    }
}
