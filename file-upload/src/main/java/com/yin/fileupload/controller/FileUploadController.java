package com.yin.fileupload.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * FileUploadController
 *
 * @author Yin Guiqing
 */
@Controller
public class FileUploadController {
    /**
     * 自定义文件保存路径
     */
    @Value("${file.upload.path}")
    private String path;

    /**
     * 去文件上传页面
     */
    @GetMapping("/")
    public String uploadPage() {
        return "FileUpload";
    }

    /**
     * 处理文件上传请求
     */
    @ResponseBody
    @PostMapping("/fileUpload")
    public String fileUpload(@RequestParam("username") String username,
                             @RequestPart("singleFile") MultipartFile singleFile,
                             @RequestPart("multipleFiles") MultipartFile[] multipleFiles) throws IOException {
        System.out.println(username + " 上传文件");
        // 处理单文件上传
        // 文件名
        String singleFileName = singleFile.getOriginalFilename();
        // 文件大小（字节）
        long singleFileSize = singleFile.getSize();
        // 保存文件，这里以放到 D: 为例
        if (!singleFile.isEmpty()) {
            singleFile.transferTo(new File(path + singleFileName));
        }
        String msg1 = "单文件上传成功！文件名：" + singleFileName
                + "，文件大小（byte）：" + singleFileSize;
        System.out.println(msg1);

        // 处理多文件上传
        // 文件个数
        int length = multipleFiles.length;
        System.out.println("多文件上传个数：" + length);
        for (MultipartFile multipleFile : multipleFiles) {
            // 文件名
            String multipleFileName = multipleFile.getOriginalFilename();
            // 文件大小（字节）
            long multipleFileSize = multipleFile.getSize();
            // 保存文件，这里以放到 D: 为例
            if (!multipleFile.isEmpty()) {
                multipleFile.transferTo(new File(path + multipleFileName));
            }
            String msg2 = "文件上传成功！文件名：" + multipleFileName
                    + "，文件大小（byte）：" + multipleFileSize;
            System.out.println(msg2);
        }
        return "上传完毕";
    }
}
